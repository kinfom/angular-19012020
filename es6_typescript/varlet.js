const x = 5;
x = 8;
// {
//   var a = 45; // function scope
// }
// {
//   let b = 56; // block scope
//   console.log(b);
// }
// console.log(a);
for (let index = 0; index < 10; index++) {
  console.log(index);
}
console.log("outside the loop", index);
