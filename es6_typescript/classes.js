// class
var Calculator = (function () {
    // constructor
    function Calculator(x, y) {
        // data members
        this.a = 9;
        this.a = x;
        this.b = y;
    }
    // member functions
    Calculator.prototype.sum = function () {
        return this.a + this.b;
    };
    Calculator.prototype.difference = function () {
        return this.a - this.b;
    };
    return Calculator;
})();
// instantiating and using the object
var calc = new Calculator(4, 5); // calc.constructor(4,5)
calc.a = 50;
// calc.b = 6;
console.log(calc.sum());
