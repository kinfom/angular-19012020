export const greeting: string = "hello world";
export let username: string = "john";
export const age: number = 45;
// const hobbies: [] = ["cycling", "gardening"];
export const address: {} = {
  door: 32,
  apartment: "my villa",
  city: "CA",
  country: "US"
};
export default function printAge() {
  console.log(`${username} is ${age} years old`);
}
