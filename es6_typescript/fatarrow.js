// function printName() {
//     console.log('john')
// }

// var printName = function()
// {
//     console.log('john')
// }

// fat arrow notation

// var printName = () => console.log("john");

// function printName(name) {
//   console.log(name);
// }

// var printName = name => console.log(name);

// function printName(fname, lname) {
//   var fullname = fname + " " + lname;
//   console.log(fullname);
// }

var printName = (fname, lname) => {
  //   fullname = "full name is " + fname + " " + lname;
  fullname = `The full name is ${fname} ${lname}`; // template literals way
  console.log(fullname);
};

printName("john", "smith");
