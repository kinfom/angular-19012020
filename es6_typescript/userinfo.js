exports.greeting = "hello world";
exports.username = "john";
exports.age = 45;
// const hobbies: [] = ["cycling", "gardening"];
exports.address = {
    door: 32,
    apartment: "my villa",
    city: "CA",
    country: "US"
};
function printAge() {
    console.log(exports.username + " is " + exports.age + " years old");
}
exports["default"] = printAge;
