// class
class Calculator {
  // data members
  a: number = 9;
  b: number;
  // constructor
  constructor(x: number, y: number) {
    this.a = x;
    this.b = y;
  }
  // member functions
  sum() {
    return this.a + this.b;
  }
  difference() {
    return this.a - this.b;
  }
}
// instantiating and using the object
const calc = new Calculator(4, 5); // calc.constructor(4,5)

calc.a = 50;
// calc.b = 6;
console.log(calc.sum());
