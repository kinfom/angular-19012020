console.log("hello world");
var a = 5; // number
a = "5";
var b = 6; // number
var x = "john"; // string
var isHuman = true; // booleans

// OBJECT -> fileds -> key value pairs
var address = {
  doorno: 405, // field
  apartment: "sunshine villa",
  street: "1st street",
  city: "new york",
  country: "US",
  greetGuest: function() {
    console.log("hello guest");
  }
};
var hobbies = ["swimming", "cycling", "playing guitar"];
address.greetGuest();
console.log(a + b);
// console.log(address.greetGuest());
// primitive datatatypes = numbers, strings, booleans
// es 6 -> let and const
