// var a = 5;
// var b = a;
// var b = 6;
// console.log(a, b);

// array immutable

var x = [1, 2, 3, 4]; // mutable
// var y = x; // just assigining the memory reference
var y = [...x]; // spread operator - copy x to y completely, making x immutable
y.push(5);
//console.log(x, y);

// object immutable

var p = { name: "joe", age: 30 };
var q = { ...p };
q.email = "joe@gmail.com";
//console.log(p, q);

function sortArguments(...arguments) {
  // rest operator
  console.log(arguments);
  return arguments.sort();
}

console.log(sortArguments(2, 6, 4, 5, 8));
