export const users = [
  { name: "john", email: "john@example.com", phone: "23424234" },
  { name: "joe", email: "joe@example.com", phone: "23424234" },
  { name: "peter", email: "peter@example.com", phone: "23424234" },
  { name: "alex", email: "alxec@example.com", phone: "23424234" },
  { name: "mary", email: "may@example.com", phone: "23424234" }
];
