import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class DateService {
  constructor() {}
  getToday() {
    const today = new Date();
    return "Today Date is: " + today;
  }
}
