import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "productfilter"
})
export class ProductfilterPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    switch (args[0]) {
      case "title":
        const titlefiltered = value.filter(product => {
          // console.log(product.title, args[0]);
          return product.title.includes(args[1]);
        });
        return titlefiltered;
        break;

      case "price":
        const pricefiltered = value.filter(product => {
          // console.log(product.title, args[0]);
          if (args[2] == 0 && args[3] == 0) {
            return value;
          }
          return (
            this.getDiscountedPrice(product.price, product.discount) >
              args[2] &&
            this.getDiscountedPrice(product.price, product.discount) < args[3]
          );
        });
        return pricefiltered;
        break;
      default:
        return value;
    }
  }
  getDiscountedPrice(price, discount) {
    return price - (price * discount) / 100;
  }
}
