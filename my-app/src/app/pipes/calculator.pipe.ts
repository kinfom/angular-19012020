import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "calculator"
})
export class CalculatorPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    console.log(args);
    switch (value) {
      case "add":
        return args[0] + args[1];
        break;
      case "sub":
        return args[0] - args[1];
        break;
      case "mul":
        return args[0] * args[1];
        break;
      case "div":
        return args[0] / args[1];
        break;
    }
    return value;
  }
}
