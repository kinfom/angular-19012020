import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-child",
  templateUrl: "./child.component.html",
  styleUrls: ["./child.component.css"]
})
export class ChildComponent implements OnInit {
  @Input() childMessage: string;

  message: string = "Hello Parent";
  price = 34343;
  date = new Date();
  number = 45.43;
  product = {
    id: 1,
    title: "Apple",
    price: 1234,
    discount: 10,
    image: "apple.jpg"
  };
  @Output() messageEvent = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {}
  sendMessage() {
    this.messageEvent.emit(this.message);
  }
}
