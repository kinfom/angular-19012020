import { Directive, ElementRef } from "@angular/core";

@Directive({
  selector: "[appHighlight]"
})
export class HighlightDirective {
  constructor(ele: ElementRef) {
    console.log(ele);
    ele.nativeElement.style.color = "red";
    ele.nativeElement.style.backgroundColor = "yellow";
    ele.nativeElement.innerText = "This is text added by custom directive";
  }
}
