import { Component, OnInit } from "@angular/core";
import { DateService } from "../services/date.service";

@Component({
  selector: "app-user-registration",
  templateUrl: "./user-registration.component.html",
  styleUrls: ["./user-registration.component.css"]
})
export class UserRegistrationComponent implements OnInit {
  fname = "";
  lname = "";
  message = "";
  error = false;
  alertClass = "alert-danger";
  datemsg = "";

  constructor(private dateservice: DateService) {}

  ngOnInit() {
    this.datemsg = this.dateservice.getToday();
  }

  registerUser(value) {
    this.message = "";
    this.error = false;
    if (!value.fname) {
      this.error = true;
      this.message += "First name is required";
      console.log("first name is required");
    }
    if (!value.lname) {
      this.error = true;
      this.message += "\nLast name is required";
      console.log("LAST name is required");
    }
    if (!this.error) {
      this.message = "Form Submitted";
      console.log(value, "form submitted");
    }
    if (!this.error) {
      this.alertClass = "alert-success";
    }
  }
}
