import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DateService } from "../services/date.service";
@Component({
  selector: "app-reactive",
  templateUrl: "./reactive.component.html",
  styleUrls: ["./reactive.component.css"]
})
export class ReactiveComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  datemsg = "";
  constructor(private fb: FormBuilder, private dateservice: DateService) {}

  ngOnInit() {
    this.datemsg = this.dateservice.getToday();
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      console.log("invalid form");
      return;
    }
    console.log("the form data is ", this.loginForm.value);
  }
  onReset() {
    this.submitted = false;
    this.loginForm.reset();
  }
}
