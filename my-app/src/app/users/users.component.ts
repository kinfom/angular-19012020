import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  users: any;
  userId: string;
  title: string;
  post: string;
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.http
      .get("https://jsonplaceholder.typicode.com/users")
      .subscribe(data => {
        console.log(data);
        this.users = data;
        console.log(this.users);
      });
  }
  registerPost(value) {
    console.log(value);
    this.http
      .post("https://jsonplaceholder.typicode.com/posts", value)
      .subscribe(
        data => {
          console.log(data);
        },
        err => console.log("hey you got an error", err)
      );
  }
}
