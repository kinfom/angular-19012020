import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipeCalculatorComponent } from './pipe-calculator.component';

describe('PipeCalculatorComponent', () => {
  let component: PipeCalculatorComponent;
  let fixture: ComponentFixture<PipeCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipeCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipeCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
