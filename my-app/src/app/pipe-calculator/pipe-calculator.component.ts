import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-pipe-calculator",
  templateUrl: "./pipe-calculator.component.html",
  styleUrls: ["./pipe-calculator.component.css"]
})
export class PipeCalculatorComponent implements OnInit {
  constructor() {}
  a: number;
  b: number;
  operation: string;
  ngOnInit() {
    this.a = 6;
    this.b = 9;
    this.operation = "mul";
  }
}
