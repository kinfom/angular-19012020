import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyModuleSecondComponent } from './my-module-second.component';

describe('MyModuleSecondComponent', () => {
  let component: MyModuleSecondComponent;
  let fixture: ComponentFixture<MyModuleSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyModuleSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyModuleSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
