import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MyModuleFirstComponent } from "./my-module-first/my-module-first.component";
import { MyModuleSecondComponent } from "./my-module-second/my-module-second.component";
import { MyModuleIndexComponent } from "./my-module-index/my-module-index.component";

const routes: Routes = [
  {
    path: "",
    component: MyModuleIndexComponent,
    children: [
      { path: "second", component: MyModuleSecondComponent },
      { path: "first", component: MyModuleFirstComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyModuleRoutingModule {}
