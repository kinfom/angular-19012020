import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyModuleRoutingModule } from './my-module-routing.module';
import { MyModuleFirstComponent } from './my-module-first/my-module-first.component';
import { MyModuleSecondComponent } from './my-module-second/my-module-second.component';
import { MyModuleIndexComponent } from './my-module-index/my-module-index.component';


@NgModule({
  declarations: [MyModuleFirstComponent, MyModuleSecondComponent, MyModuleIndexComponent],
  imports: [
    CommonModule,
    MyModuleRoutingModule
  ]
})
export class MyModuleModule { }
