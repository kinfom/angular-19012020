import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyModuleIndexComponent } from './my-module-index.component';

describe('MyModuleIndexComponent', () => {
  let component: MyModuleIndexComponent;
  let fixture: ComponentFixture<MyModuleIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyModuleIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyModuleIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
