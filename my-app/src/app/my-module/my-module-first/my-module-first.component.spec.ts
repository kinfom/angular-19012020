import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyModuleFirstComponent } from './my-module-first.component';

describe('MyModuleFirstComponent', () => {
  let component: MyModuleFirstComponent;
  let fixture: ComponentFixture<MyModuleFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyModuleFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyModuleFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
