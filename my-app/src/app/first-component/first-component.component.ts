import { Component, OnInit, Input, OnDestroy } from "@angular/core";

@Component({
  selector: "app-first-component",
  templateUrl: "./first-component.component.html",
  styleUrls: ["./first-component.component.css"]
})
export class FirstComponentComponent implements OnInit, OnDestroy {
  name: string;
  a: any;
  b: any;

  constructor() {
    console.log("constructor called for first component");
  }

  ngOnInit() {
    this.name = "John";
    console.log("on init called for first component");
  }
  ngAfterViewInit() {
    console.log("ngAfterViewInit called for first component");
  }

  greetUser() {
    console.log("hello " + this.name);
    let sum = parseInt(this.a) + parseInt(this.b);
    console.log(sum);
  }
  inputHandler(e) {
    console.log(e.target.value);
    this.name = e.target.value;
  }
  ngOnDestroy() {
    console.log("on destroy called for first component");
  }
}
