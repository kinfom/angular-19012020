import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { FirstComponentComponent } from "./first-component/first-component.component";
import { UserRegistrationComponent } from "./user-registration/user-registration.component";
import { ReactiveComponent } from "./reactive/reactive.component";
import { ProductsComponent } from "./products/products.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { DetailComponent } from "./products/detail/detail.component";
import { PipeCalculatorComponent } from "./pipe-calculator/pipe-calculator.component";
import { ParentComponent } from "./parent/parent.component";
import { MyModuleIndexComponent } from "./my-module/my-module-index/my-module-index.component";
import { MyModuleFirstComponent } from "./my-module/my-module-first/my-module-first.component";
import { MyModuleSecondComponent } from "./my-module/my-module-second/my-module-second.component";
import { UsersComponent } from "./users/users.component";
const routes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  {
    path: "home",
    component: FirstComponentComponent,
    children: [
      { path: "second", component: PipeCalculatorComponent },
      { path: "pipes", component: ReactiveComponent },
      { path: "parent", component: ParentComponent }
    ]
  },
  { path: "template-driven-forms", component: UserRegistrationComponent },
  { path: "reactive-forms", component: ReactiveComponent },
  { path: "products", component: ProductsComponent },
  { path: "product/:id", component: DetailComponent },
  { path: "users", component: UsersComponent },
  // {
  //   path: "mymodule",
  //   component: MyModuleIndexComponent,
  //   children: [
  //     { path: "first", component: MyModuleFirstComponent },
  //     { path: "second", component: MyModuleSecondComponent }
  //   ]
  // },
  // { path: "mymodule", loadChildren: "./my-module.module#MyModuleModule" },
  {
    path: "mymodule",
    loadChildren: () =>
      import("./my-module/my-module.module").then(m => m.MyModuleModule)
  },

  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
