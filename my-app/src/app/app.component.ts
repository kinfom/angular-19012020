import { Component, OnInit } from "@angular/core";
import { users } from "./../assets/users";
@Component({
  selector: "app-root",
  // template: "<h1>My app</h1>", // inline template
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  constructor() {
    console.log("constructor called");
  }
  title = "my-first-app";
  appMessage = "I am from app component";
  canTalk: string = "false";
  numbers: number[] = [1, 2, 3, 4, 5, 6];
  users = users;
  showFirst = false;
  classes = "h2 text-center text-warning shadowed-text";
  applyShadow = true;
  colClasses = "col-md-3";
  ngOnInit() {
    console.log("oninit called");
    console.log(users);
  }
  ngOnDestroy() {
    console.log("ondestroy called");
  }
  ngAfterViewInit() {
    console.log("ngAfterViewInit called");
  }
  toggleFirst = () => (this.showFirst = !this.showFirst);
  toggleShadow = () => (this.applyShadow = !this.applyShadow);
}
