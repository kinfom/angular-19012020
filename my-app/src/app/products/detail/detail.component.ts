import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { products } from "./../../../assets/products";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.css"]
})
export class DetailComponent implements OnInit {
  constructor(private router: ActivatedRoute) {}
  id: number;
  products = products;
  productinfo: any;

  ngOnInit() {
    this.router.params.subscribe(res => {
      console.log(res);
      this.id = res.id;
      this.productinfo = products.filter(product => product.id == this.id);
    });
  }
}
