import { Component, OnInit } from "@angular/core";
import { products } from "./../../assets/products";
@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.css"]
})
export class ProductsComponent implements OnInit {
  products = products;
  key = "";
  filter = "";
  min = 0;
  max = 0;
  productbg = "white";
  colClasses = "col-md-3 product";
  productClasses = "product";

  constructor() {}

  ngOnInit() {}

  // filterProducts(e) {
  //   console.log(e.target.value);
  //   let key = e.target.value;
  //   this.filteredproducts = products.filter(product => {
  //     product.title.includes(key);
  //   });
  // }
}
