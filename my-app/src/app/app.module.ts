import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FirstComponentComponent } from "./first-component/first-component.component";
import { NavBarComponent } from "./nav-bar/nav-bar.component";
import { ParentComponent } from "./parent/parent.component";
import { ChildComponent } from "./child/child.component";
import { CeilPipe } from "./pipes/ceil.pipe";
import { PipeCalculatorComponent } from "./pipe-calculator/pipe-calculator.component";
import { CalculatorPipe } from "./pipes/calculator.pipe";
import { ProductsComponent } from "./products/products.component";
import { DiscountPipe } from "./discount.pipe";
import { ProductfilterPipe } from "./pipes/productfilter.pipe";
import { HighlightDirective } from "./highlight.directive";
import { UserRegistrationComponent } from "./user-registration/user-registration.component";
import { ReactiveComponent } from "./reactive/reactive.component";
import { DateService } from "./services/date.service";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { DetailComponent } from "./products/detail/detail.component";
import { UsersComponent } from './users/users.component';
import { TestingComponent } from './testing/testing.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstComponentComponent,
    NavBarComponent,
    ParentComponent,
    ChildComponent,
    CeilPipe,
    PipeCalculatorComponent,
    CalculatorPipe,
    ProductsComponent,
    DiscountPipe,
    ProductfilterPipe,
    HighlightDirective,
    UserRegistrationComponent,
    ReactiveComponent,
    PageNotFoundComponent,
    DetailComponent,
    UsersComponent,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [DateService],
  bootstrap: [AppComponent]
})
export class AppModule {}
